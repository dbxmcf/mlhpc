#!/usr/bin/env python
#http://stackoverflow.com/questions/28931224/adding-value-labels-on-a-matplotlib-bar-chart
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt

font = {'family':'normal','weight':'bold','size': 20}

matplotlib.rc('font', **font)

#  2 CPU-only 537
#  3 GPU-Theano 47
#  4 GPU-Tensorflow 23
frequencies = [537, 47, 23]   # bring some raw data

freq_series = pd.Series.from_array(frequencies)   # in my original code I create a series and run on that, so for consistency I create a series from the list.

#x_labels = [108300.0, 110540.0, 112780.0, 115020.0, 117260.0, 119500.0, 121740.0, 123980.0, 126220.0, 128460.0, 130700.0]
x_labels = ['CPU Only','GPU-Theano','GPU-Tensorflow']

nvidia_green=(116./255.,183./255.,27./255.)
# now to plot the figure...
plt.figure(figsize=(12, 8))
ax = freq_series.plot(kind='bar')
#ax.get_children()[2].set_color('g') 
ax.get_children()[1].set_color(nvidia_green) 
ax.get_children()[2].set_color(nvidia_green) 
#ax.set_title("Performance Comparison")
#ax.set_xlabel("Architecture")
ax.set_ylabel("Runtime (sec)")
#ax.set_xticklabels(x_labels, rotation=45)
ax.set_xticklabels(x_labels, rotation=0)

rects = ax.patches

# Now make some labels
#labels = ["label%d" % i for i in xrange(len(rects))]
labels = ['537','47','23']

for rect, label in zip(rects, labels):
    height = rect.get_height()
    ax.text(rect.get_x() + rect.get_width()/2, height + 5, label, ha='center', va='bottom')

plt.show()

