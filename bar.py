#!/usr/bin/env python
import matplotlib.pyplot as plt

fig = plt.figure()            #create a figure
ax = fig.add_subplot(1,1,1)    # add plot
x = [2,4,6,8]         #x axis value
y = [4,8,12,16]   #Y axis: height
ax.bar(x,y, align='center', color='green') #required ax.bar(x,y)
plt.show()   
