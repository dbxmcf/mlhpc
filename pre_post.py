import numpy as np
import matplotlib.pyplot as plt

def preview_train(X_train,y_train,start=0,block=True):
    #start=100
    fig = plt.figure() 
    fig.canvas.set_window_title('Review the Training data:') 
    for i in range(9):
        plt.subplot(3,3,i+1)
        plt.imshow(X_train[start+i], cmap='gray', interpolation='none')
        plt.title("label: {}".format(y_train[start+i]))
    plt.tight_layout()
    plt.show(block=block)

def visu_results(X_test,y_test,model):
    predicted_classes = model.predict_classes(X_test)
    
    # Check which items we got right / wrong
    correct_indices = np.nonzero(predicted_classes == y_test)[0]
    incorrect_indices = np.nonzero(predicted_classes != y_test)[0]
    
    fig = plt.figure() 
    fig.canvas.set_window_title('Correct Predictions') 
    for i, correct in enumerate(correct_indices[:9]):
        plt.subplot(3,3,i+1)
        plt.imshow(X_test[correct].reshape(28,28), cmap='gray', interpolation='none')
        plt.title("Predicted {}, label {}".format(predicted_classes[correct], y_test[correct]))
    plt.tight_layout()
    
    fig = plt.figure() 
    fig.canvas.set_window_title('Incorrect Predictions') 
    for i, incorrect in enumerate(incorrect_indices[:9]):
        plt.subplot(3,3,i+1)
        plt.imshow(X_test[incorrect].reshape(28,28), cmap='gray', interpolation='none')
        plt.title("Predicted {}, label {}".format(predicted_classes[incorrect], y_test[incorrect]))
    plt.tight_layout()

