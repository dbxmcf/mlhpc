import matplotlib.pyplot as plt
import keras
import multiprocessing as mp

class LossHistory(keras.callbacks.Callback):
    accs=[]
    def __init__(self):
        #self.queue = mp.JoinableQueue()
        self.queue = mp.Queue()
        self.pjob = mp.Process(target=self.plot_init,args=(self.queue,))
        self.pjob.daemon = True
        self.pjob.start()
        self.queue.put(1)
        

    def plot_init(self,q):
        #plt.plot(self.epoches,self.accs,'r-s',markersize=5,label='training acc')
        #plt.plot(self.epoches,self.val_accs,'b-o',markersize=5,label='test acc')
        #plt.show(block=True)
        #plt.pause(1)
        while True:
            signal = q.get(block=True)
            #print signal
            if signal==1: 
                print('started signal=',signal)
                plt.ion()
                self.figure, self.ax = plt.subplots()
                self.lines, = self.ax.plot([],[], 'o')
                #Autoscale on unknown axis and known lims on the other
                self.ax.set_autoscaley_on(True)
                #self.ax.set_xlim(self.min_x, self.max_x)
                self.ax.set_xlim(0, 20)
                #Other stuff
                self.ax.grid()
                #self.figure, self.ax = plt.subplots()
                #self.lines, = self.ax.plot([],[], 'o')
                ##Autoscale on unknown axis and known lims on the other
                #self.ax.set_autoscaley_on(True)
                #self.ax.set_xlim(self.min_x, self.max_x)
                ##Other stuff
                #self.ax.grid()
                #plt.plot([],[])
                #plt.xlabel('epoch')
                #plt.ylabel('accuracy %')
                #plt.legend(loc='lower right')
                #plt.show(block=True)
                plt.show(block=False)
            if signal == 0:
                print('recvd signal',signal)
                break
            else:
                #print('else signal',len(list(signal)))
                print('else signal')
                #Update data (with the new _and_ the old points)
                #self.lines.set_xdata(range(len(signal)))
                self.lines.set_ydata(signal)
                #Need both of these in order to rescale
                self.ax.relim()
                self.ax.autoscale_view()
                #We need to draw *and* flush
                self.figure.canvas.draw()
                self.figure.canvas.flush_events()
                #print('signal len=',len(self.accs))
                #print('signal len=',myaccs)
                #print('signal len=',len(signal))
                #plt.hold(True)
                #plt.plot(signal,'b-o',markersize=5)
                #print('accs=',self.accs)
                #print('epoches=',self.epoches)
                #plt.plot(self.epoches,self.losses,'b-o',markersize=5)
                #plt.plot(self.epoches,self.accs,'r-s',markersize=5)
                #plt.plot(self.epoches,self.val_accs,'b-o',markersize=5)
                #plt.plot(self.epoches,self.val_accs,'b-',markersize=5)
                #plt.draw()
                #plt.flush_events()
                #plt.pause(0.0001)
    #return plot_init
        
    def on_train_begin(self, logs={}):
        self.losses = []
        #self.accs = []
        self.val_accs = []
        self.epoches = []

    def on_train_end(self, logs={}):
        # Wait for the worker to finish
        self.queue.put(0)
        self.queue.close()
        #self.queue.join_thread()
        #self.pjob.join()
        if self.pjob.is_alive():
            self.pjob.terminate()
        print "terminated"
        self.pjob.join()
        return

    def on_batch_end(self, epoch, logs={}):
        #self.losses.append(logs.get('loss'))
        #self.queue.put(2)
        self.accs.append(logs.get('acc'))
        #print len(self.accs)
        self.queue.put(self.accs)
        #self.val_accs.append(logs.get('val_acc'))
        #self.epoches.append(epoch)
        #self.queue.put(self.accs)
        #print self.accs
        #plt.plot(logs.get('loss'))
        #plt.draw()
        #loss_value=logs.get('loss')
        #plot(epoch,loss_value,'b-o',markersize=10)

    #def on_batch_end(self, batch, logs={}):
    #    self.losses.append(logs.get('loss'))
    #    self.accs.append(logs.get('acc'))
    #    self.val_accs.append(logs.get('val_acc'))
    #    #self.epoches.append(epoch)
    #    #plt.plot(logs.get('loss'))
    #    #plt.draw()
    #    loss_value=logs.get('loss')
    #    #plot(epoch,loss_value,'b-o',markersize=10)
    #    plt.hold(True)
    #    #plt.plot(self.epoches,self.losses,'b-o',markersize=5)
    #    #plt.plot(self.epoches,self.accs,'r-s',markersize=5)
    #    #plt.plot(self.epoches,self.val_accs,'b-o',markersize=5)
    #    plt.plot(self.accs,'r--',markersize=5)
    #    plt.plot(self.val_accs,'b-o',markersize=5)
    #    plt.draw()
    #    plt.pause(0.01)


