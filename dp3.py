import matplotlib.pyplot as plt
import keras
plt.ion()

class DynamicUpdate(keras.callbacks.Callback):
    #Suppose we know the x range
    min_x = 0
    max_x = 500

    def on_launch(self):
        #Set up plot
        #self.figure, self.ax = plt.subplots(1,2,sharey=True)
        fs=20.0
        #plt.tick_params(axis='both', which='major', labelsize=fs)
        #plt.tick_params(axis='both', which='minor', labelsize=fs)

        self.figure, self.ax = plt.subplots(1,2,figsize=(16, 8),sharey=False)
        self.figure.patch.set_facecolor('white')
        lw=2.0
        self.lines0, = self.ax[0].plot([0],[0], 'r-', linewidth=lw, label='test acc')
        self.lines1, = self.ax[0].plot([0],[0], 'b-', linewidth=lw, label='training acc')
        self.lines2, = self.ax[1].plot([0],[0], 'b-', linewidth=lw, label='training loss')
        self.lines3, = self.ax[1].plot([0],[0], 'r-', linewidth=lw, label='test loss')
        #self.lines, = self.ax.plot([], 'r-')
        #Autoscale on unknown axis and known lims on the other
    
        self.ax[0].set_autoscaley_on(True)
        self.ax[0].grid()
        self.ax[0].set_xlabel('epoch',fontsize = fs)
        self.ax[0].set_ylabel('accuracy',fontsize = fs)
        self.ax[0].legend(shadow=True, fancybox=True,loc='lower right',fontsize=fs)
        self.ax[0].set_title('Accuracy',fontweight="bold", fontsize=fs) 
        self.ax[0].tick_params(axis='both', labelsize=fs)
        #self.ax[0].set_axis_bgcolor('white')
        #self.ax[0].patch.set_facecolor('white')
        #self.ax[0].set_xticklabels(fontsize=fs)
        #self.ax[0].set(adjustable='box-forced', aspect='equal')
        #self.ax[0].set(aspect='equal')
        #plt.tick_params(axis='both', which='major', labelsize=fs)
        #plt.tick_params(axis='both', which='minor', labelsize=fs)
        self.ax[1].set_autoscaley_on(True)
        self.ax[1].grid()
        self.ax[1].set_xlabel('epoch',fontsize = fs)
        self.ax[1].set_ylabel('loss',fontsize = fs)
        self.ax[1].legend(shadow=True, fancybox=True,fontsize=fs)
        self.ax[1].set_title('Cross entropy loss',fontweight="bold", fontsize=fs) 
        self.ax[1].tick_params(axis='both', labelsize=fs)
        #self.ax[1].set_axis_bgcolor('white')
        #self.ax[1].patch.set_facecolor('white')
        #self.ax[1].set_xticklabels(fontsize=fs)
        #self.ax[1].set(adjustable='box-forced', aspect='equal')
        #self.ax[1].set(aspect='equal')
        #plt.show()
        #self.ax.set_xlim(self.min_x, self.max_x)
        #Other stuff

    #def on_running(self, xdata, ydata):
    def on_running(self, accs,val_accs,losses,val_losses):
        #Update data (with the new _and_ the old points)
        
        #self.lines.set_xdata(xdata)
        self.lines1.set_xdata(range(len(accs)))
        self.lines1.set_ydata(accs)
        self.lines0.set_xdata(range(len(val_accs)))
        self.lines0.set_ydata(val_accs)

        self.lines2.set_xdata(range(len(losses)))
        self.lines2.set_ydata(losses)
        self.lines3.set_xdata(range(len(val_losses)))
        self.lines3.set_ydata(val_losses)
        #Need both of these in order to rescale
        self.ax[0].relim()
        self.ax[0].autoscale_view()
        self.ax[1].relim()
        self.ax[1].autoscale_view()
        #We need to draw *and* flush
        #self.figure.gca().set_aspect('equal', adjustable='box')
        self.figure.canvas.draw()
        self.figure.canvas.flush_events()

    def on_train_begin(self, logs={}):
        self.on_launch()
        self.losses = []
        self.accs = []
        self.val_losses = []
        self.val_accs = []

    def on_epoch_end(self, batch, logs={}):
    #def on_batch_end(self, batch, logs={}):
        self.losses.append(logs.get('loss'))
        self.accs.append(logs.get('acc'))
        self.val_losses.append(logs.get('val_loss'))
        self.val_accs.append(logs.get('val_acc'))
        #self.on_running(xdata, ydata)
        self.on_running(self.accs,self.val_accs,self.losses,self.val_losses)

    def __init__(self):
        import numpy as np
        import time
        #self.on_launch()
        #xdata = []
        #ydata = []
        ##for x in np.arange(0,10,0.5):
        ##    xdata.append(x)
        ##    ydata.append(np.exp(-x**2)+10*np.exp(-(x-7)**2))
        ##    self.on_running(xdata, ydata)
        ##    #time.sleep(1)
        #return xdata, ydata
        

    #Example
    #def __call__(self):

#d = DynamicUpdate()
