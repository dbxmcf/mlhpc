
# coding: utf-8

# In[1]:

# In[2]:

import tensorflow as tf
sess = tf.Session()

from keras import backend as K
K.set_session(sess)
K.set_image_dim_ordering('th')

import cPickle as pickle

from keras.models import Sequential
#from keras.layers import Convolution2D, Flatten, Dense, Reshape, Activation, GaussianNoise, UpSampling2D
from keras.layers import Convolution2D, Flatten, Dense, Reshape, Activation, GaussianNoise
from keras.layers import Deconvolution2D
from keras.utils.layer_utils import print_summary


# In[3]:

from keras.datasets import mnist
(X_train, y_train), (X_test, y_test) = mnist.load_data()


# In[4]:
import numpy as np
X_train = X_train.astype("float32") / 255.0
X_test = X_test.astype("float32") / 255.0
X_train = X_train.reshape((X_train.shape[0], 1, X_train.shape[1], X_train.shape[2]))
X_test = X_test.reshape((X_test.shape[0], 1, X_test.shape[1], X_test.shape[2]))
X_train -= np.mean(X_train)
X_test -= np.mean(X_test)


# In[5]:

img_x, img_y = X_train.shape[2:]
n_emb = 30
atv = 'tanh'


# In[6]:

f_e = Sequential()
f_e.add(Convolution2D(20, 5, 5, input_shape=(1, img_x, img_y), border_mode='same'))
f_e.add(Activation(atv))
f_e.add(Convolution2D(20, 5, 5, border_mode='same'))
f_e.add(Activation(atv))
f_e.add(Flatten())
f_e.add(Dense(n_emb, activation=atv))


# In[7]:

f_e.compile(loss='mean_squared_error', optimizer='rmsprop')
print_summary(f_e.layers)


# In[8]:
f_d = Sequential()
f_d.add(Dense(20*img_x*img_y, input_dim=n_emb, activation=atv))
f_d.add(Reshape((20, img_x, img_y)))
f_d.add(Deconvolution2D(20, 5, 5, output_shape=(None, 20, img_x, img_y), border_mode='same'))
f_d.add(Activation(atv))
f_d.add(Deconvolution2D(1, 5, 5, output_shape=(None, 1, img_x, img_y), border_mode='same'))
f_d.add(Activation(atv))

# f_d = Sequential()
# f_d.add(Dense(20*7*7, input_dim=n_emb, activation=atv))
# f_d.add(Reshape((20, 7, 7)))
# f_d.add(Convolution2D(20, 5, 5, activation='relu', border_mode='same'))
# f_d.add(UpSampling2D((2, 2)))
# f_d.add(Convolution2D(20, 5, 5, activation='relu', border_mode='same'))
# f_d.add(UpSampling2D((2, 2)))
# f_d.add(Convolution2D(1, 5, 5, activation='sigmoid', border_mode='same'))
# f_d.add(Reshape((1, img_x, img_y)))


# In[9]:

f_d.compile(loss='mean_squared_error', optimizer='rmsprop')
print_summary(f_d.layers)


# In[10]:

model = Sequential()
model.add(GaussianNoise(0.01, input_shape=(1, img_x, img_y)))
model.add(f_e)
model.add(f_d)


# In[11]:

model.compile(loss='mean_squared_error', optimizer='rmsprop') 
print_summary(model.layers)


# In[12]:

h = model.fit(X_train, X_train, batch_size=512, nb_epoch=100)


# In[13]:

#plot(h.history.values()[0][10:])


# In[16]:

ix = 12
pp = model.predict(X_train[ix][None, :, :, :])
emb = f_e.predict(X_train[ix][None, :, :, :])

# figure(figsize=(9, 3))
# subplot(1, 3, 1)
# imshow(X_train[ix].reshape((img_x, img_y)))
# subplot(1, 3, 2)
# plot(emb[0], 'o')
# axis([0, 30, -1.1, 1.1])
# subplot(1, 3, 3)
# imshow(pp[0].reshape((img_x, img_y)))


# In[18]:

pickle.dump(model.get_config(), open('Vauto_model.c', 'wb'))
model.save_weights('Vauto_model.w', overwrite=True)


# In[ ]:



