#from matplotlib.mlab import normpdf
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

#matplotlib.rc('text', usetex = True)
#matplotlib.rc('font', **{'family' : "sans-serif"})
#params = {'text.latex.preamble' : [r'\usepackage{siunitx}', r'\usepackage{amsmath}']}
#p.rcParams.update(params)
#plt.rcParams["figure.figsize"] = (20,3)
fig=plt.figure(figsize=(6,3))
x = np.arange(-10, 10, 0.1)
y = 1.0/(1+np.exp(-x))
#p.plot(x,y, color='red', lw=3, label=r'$y=\displaystyle\frac{{1}}{1+e^{z}}$')
plt.plot(x,y, color='red', lw=3, label=r'$y=\dfrac{{1}}{1+e^{z}}$')
#p.legend(loc='lower right',fontsize=20)
plt.grid()
plt.show()

x = [-10,0,0,10] 
y = [0., 0.0, 1.0, 1.0]
fig=plt.figure(figsize=(6,3))

#plt.step(x, y)
plt.step(x,y, color='red', lw=3, label=r'$y=\dfrac{{1}}{1+e^{z}}$')
plt.grid()
plt.show()
